package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectWarehouse;

public class AbstractDAO<T> {
	  protected final Class<T> type;
	//@SuppressWarnings("unchecked")
	public AbstractDAO()
	{
		this.type=(Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	public T get_by_name(String name) throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException
	{
		String query="Select * from Warehouse."+type.getSimpleName()+" Where name=?";
		Connection conn=null;
		System.out.println(query+" "+name);
		PreparedStatement statement=null;
		conn=ConnectWarehouse.getConnection();
		ResultSet rs=null;
		try {
			statement=conn.prepareStatement(query);
			statement.setString(1, name);;
			 rs=statement.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		T instance=null;
		try {
			instance = type.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(rs.next())
		{
		for(Field field:type.getDeclaredFields())
		{
			field.setAccessible(true);
			//System.out.println(field.getName());
			Object value=rs.getObject(field.getName());
			PropertyDescriptor propertyDescriptor=new PropertyDescriptor(field.getName(),type);
			Method method=propertyDescriptor.getWriteMethod();
			method.setAccessible(true);
			//method.invoke(instance, value);
			//System.out.println(" " +field.getName()+"  " +value.getClass());
			if(field.getType().equals(String.class))
				{
				
				method.invoke(instance, (String)value);
				}
			else
			if(field.getType().equals(int.class))
				{
				method.invoke(instance,(Integer) value);
				System.out.println("");
				}
		}
		}
		return instance;
		
	}
	
	protected List<T> createObjects(ResultSet resultSet)
	{
		List <T> list=new ArrayList<T>();
		
			try {
				while(resultSet.next())
				{
					T instance=type.newInstance();
					for(Field field:type.getDeclaredFields())
					{
						field.setAccessible(true);
						//System.out.println(field.getName());
						Object value=resultSet.getObject(field.getName());
						PropertyDescriptor propertyDescriptor=new PropertyDescriptor(field.getName(),type);
						Method method=propertyDescriptor.getWriteMethod();
						method.setAccessible(true);
						//method.invoke(instance, value);
						//System.out.println(" " +field.getName()+"  " +value.getClass());
						if(field.getType().equals(String.class))
							{
							
							method.invoke(instance, (String)value);
							}
						else
						if(field.getType().equals(int.class))
							{
							method.invoke(instance,(Integer) value);
							System.out.println("");
							}
					}
					list.add(instance);
					
				}
				
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IntrospectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return list;
		
	}
	private String createQuery(String field)
	{
		StringBuilder sb=new StringBuilder();
		sb.append("Select");
		sb.append("*");
		sb.append("FROM Warehouse.");
		sb.append(type.getSimpleName());
		return sb.toString();
	}
	private String update_query(T obj)
	{	int id=0;
		String query="Update  Warehouse."+type.getSimpleName()+" Set";
		for(Field f:obj.getClass().getDeclaredFields())
		{
			f.setAccessible(true);
			
			
				query+=" "+f.getName()+"=?,";
			if(f.getName()=="id")
			{
				try {
					id=f.getInt(obj);
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
		}
		query=query.substring(0,query.length()-1);
		query=query+"\n where id="+id;
		System.out.println(query);
		return query;
		
		
		
	}
	
	public int get_max_id() throws SQLException
	{
		Connection conn=null;
		PreparedStatement statement=null;
		ResultSet rs=null;
		conn=ConnectWarehouse.getConnection();
		String query="Select max(id) from Warehouse."+type.getSimpleName();
		System.out.println(query);
		//System.out.println(query);
		try {
			statement=conn.prepareStatement(query);
		
			rs=statement.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int result=0;
		if(rs.next())
		{
			
			 result =rs.getInt(1);
		}
		else 
		{
			result= -1;
		}
		
		ConnectWarehouse.close(conn);
		ConnectWarehouse.close(statement);
		return result;
	
	}
	private String insert_query(T obj)
	{
		String query="INSERT INTO Warehouse."+type.getSimpleName()+"\nValues(";
		for(Field f:obj.getClass().getDeclaredFields())
		{
			f.setAccessible(true);
			
			
				query+="?,";
		
		}
		query=query.substring(0,query.length()-1);
		query+=");";
		System.out.println(query);
		return query;	
	}
	public void update_tuple(T obj) throws IllegalArgumentException, IllegalAccessException, SQLException
	{
		Connection conn=null;
		PreparedStatement statement=null;
		
		conn=ConnectWarehouse.getConnection();
		String query=update_query(obj);
		//System.out.println(query);
		try {
			statement=conn.prepareStatement(query);
			statement=insert_params(obj,statement);
			statement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ConnectWarehouse.close(conn);
		ConnectWarehouse.close(statement);
		
	}
	private PreparedStatement insert_params(T obj,PreparedStatement statement) throws IllegalArgumentException, IllegalAccessException, SQLException
	{
		int i=1;
		for(Field f:obj.getClass().getDeclaredFields())
		{
			f.setAccessible(true);
			Object tmp=f.get(obj);
//			
			
			if(f.getType().equals(String.class))
			{
				statement.setString(i,(String) tmp);
				System.out.println("shtring "+(String) tmp);
			}
			if(f.getType().equals(int.class))
			{
				
				statement.setInt(i, (Integer) tmp);
				System.out.println("int "+i+" "+(Integer) tmp);
			}
			i++;
		}
		return statement;
		
	}
	private String delete_query(int id)
	{
		return "Delete from Warehouse."+type.getSimpleName()+" Where ID=?";
		
	}
	public void delete_by_id(int id)
	{
		Connection conn=null;
		PreparedStatement statement=null;
		conn=ConnectWarehouse.getConnection();
		String query=delete_query(id);
		System.out.println(query);
		try {
			statement=conn.prepareStatement(query);
			statement.setInt(1,id);
			
			statement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ConnectWarehouse.close(conn);
		ConnectWarehouse.close(statement);
	
		
	}
	public void insert_tuple(T obj) throws IllegalArgumentException, IllegalAccessException
	{
		Connection conn=null;
		PreparedStatement statement=null;
		conn=ConnectWarehouse.getConnection();
			try {
				statement=conn.prepareStatement(this.insert_query(obj));
				statement=insert_params(obj,statement);
				
				//System.out.println(statement.toString());
				statement.executeUpdate();
				//statement.executeUpdate("INSERT INTO Order values(1,2,3,4)");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ConnectWarehouse.close(conn);
			ConnectWarehouse.close(statement);
		
		
	}
	
	
	public List <T> getAll()
	{
		List <T> list=new ArrayList<T>();
		Connection conn=null;
		PreparedStatement statement=null;
		ResultSet resultSet=null;
		String query="Select * from Warehouse."+type.getSimpleName();
		try
		{
			conn=ConnectWarehouse.getConnection();
			statement=conn.prepareStatement(query);
			resultSet=statement.executeQuery();
			list= this.createObjects(resultSet);
		}
		catch (SQLException e)
		{
			System.out.println("Sql connection error");
		}
		ConnectWarehouse.close(conn);
		ConnectWarehouse.close(statement);
		ConnectWarehouse.close(resultSet);
		
		return list;
	}

}
