package interface_handle;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.IntrospectionException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import bll.CustomerDAO;
import bll.OrderDAO;
import bll.ProductDAO;
import dao.AbstractDAO;
import model.Customer;
import model.Entry;
import model.Order;
import model.Product;

public class GUI {

	private JFrame main_frame;
	private JFrame client_frame;
	private JFrame admin_frame;
	private AbstractDAO dao;
	
	private DefaultTableModel create_model(List <Object> list)
	{
		
		
		int nr_fields=0;
		// Create a couple of columns 
		ArrayList<String> colls=new ArrayList<String>();
		
		if(list.size()>0)
		{
			for(Field field:list.get(0).getClass().getDeclaredFields())
			{
				nr_fields++;
				colls.add(field.getName());
			
				
			}
		}
		DefaultTableModel model = new DefaultTableModel(colls.toArray(),0); 
	
		
		for(Object aux:list)
		{
			int current=0;
			Object values[]=new Object[nr_fields];
			for(Field field:aux.getClass().getDeclaredFields())
			{
				field.setAccessible(true);
				try {
					String val=field.get(aux).toString();
					values[current]=val;
					current++;
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			model.addRow( values);
		}

		return model;
	
		
	}
	
	public void setup_gui() throws IllegalArgumentException, IllegalAccessException
	 {
			 JPanel panel;
		 main_frame = new JFrame("Warehouse");
			main_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			main_frame.setSize(200, 500);
			main_frame.setLocation(300, 300);	//setup the frame dimensions
			panel = new JPanel();
			
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			JButton aux=new JButton("Products Interface");
			 aux.addActionListener(new ActionListener()		//Delete function
					 {
				 public void actionPerformed(ActionEvent e)
				 {
					 interact_db(Product.class);
				 }
					 }
			 );
			panel.add(aux);
			 aux=new JButton("Customers Interface");
			 aux.addActionListener(new ActionListener()		//Delete function
					 {
				 public void actionPerformed(ActionEvent e)
				 {
					 interact_db(Customer.class);
				 }
					 }
			 );
				panel.add(aux);

			 aux=new JButton("Orders Interface");
			 aux.addActionListener(new ActionListener()		//Delete function
					 {
				 public void actionPerformed(ActionEvent e)
				 {
					 order();
				 }
					 }
			 );
				panel.add(aux);

			//start_client();
			//interact_db(Order.class);
			JButton but=new JButton("List bills");
			but.addActionListener(new ActionListener()		//Delete function
					 {
				 public void actionPerformed(ActionEvent e)
				 {
					 OrderDAO aux=new OrderDAO();
					 try {
						aux.create_bills();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IllegalAccessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IllegalArgumentException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (InvocationTargetException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IntrospectionException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 
				 }
					 }
			 );
			panel.add(but);
			main_frame.add(panel);
			main_frame.setVisible(true);
			
//			

	 }
	JFrame order_frame;
	JTextField product;
	JTextField customer;
	JTextField quantity;
	JLabel label;
	JTable orders_t;
	private void order()
	{
		JTable products_t;
		JTable customers_t;
		
		product=new JTextField();
		product.setMinimumSize(new Dimension(100,30));
		 product.setPreferredSize(new Dimension(100,30));
		
		customer=new JTextField();
		customer.setMinimumSize(new Dimension(100,30));
		 customer.setPreferredSize(new Dimension(100,30));
		 quantity=new JTextField();
			quantity.setMinimumSize(new Dimension(100,30));
			 quantity.setPreferredSize(new Dimension(100,30));
		ProductDAO products=new ProductDAO();
		CustomerDAO customers=new CustomerDAO();
		OrderDAO orders=new OrderDAO();
		order_frame=new JFrame("Client Operations");
		order_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		order_frame.setSize(700, 1000);
		order_frame.setLocation(300, 300);	//se
		JPanel panel= new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		List<Object> p_entries=new ArrayList<>((products.getAll())); 
		 products_t=new JTable(create_model(p_entries));
		 products_t.setSize(new Dimension(500,200));
		products_t.addMouseListener(new MouseAdapter()
		 {
		 public void mouseClicked(MouseEvent e) {
		     if (e.getClickCount() == 1) { // check if a double click
		 try{
		    	product.setText(products_t.getValueAt(products_t.getSelectedRow(), 1).toString());
		 }
		    	 catch(NullPointerException t)
		    	 {
		    		 System.out.print("No value selected\n");
		    	 }
		     }
		   }
		});
		 List<Object> c_entries=new ArrayList<>((customers.getAll()));
		  customers_t=new JTable(create_model(c_entries));
		  customers_t.addMouseListener(new MouseAdapter()
			 {
				 public void mouseClicked(MouseEvent e) {
				     if (e.getClickCount() == 1) { // check if a double click
				    	 try{
				    	customer.setText(customers_t.getValueAt(customers_t.getSelectedRow(), 1).toString());
				    	 }
				    	 catch(NullPointerException t)
				    	 {
				    		 System.out.print("No value selected\n");
				    	 }
				     }
				   }
				});
		  List<Object> o_entries=new ArrayList<>((orders.getAll()));
		  orders_t=new JTable(create_model(o_entries));
		  JScrollPane aux=new JScrollPane(products_t);
		  aux.setPreferredSize(new Dimension(500,200));
		  panel.add(aux);
		
		  aux=new JScrollPane(customers_t);
		  aux.setViewportBorder(null);
		  aux.setPreferredSize(new Dimension(500,200));
		  panel.add(aux);
		  JPanel input_panel=new JPanel();
		  
		  input_panel.add(customer);
		  input_panel.add(product);
		  input_panel.add(quantity);
			
			label=new JLabel("");
		 		  JButton but=new JButton("Insert Command");
		  but.addActionListener(new ActionListener()		//Delete function
					 {
				 public void actionPerformed(ActionEvent e)
				 {
					 String product_name=product.getText();
					 int stock=0;
					try {
						stock = products.get_stock(product_name);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 int desired=0;
					 try
					 {
						 desired=Integer.parseInt(quantity.getText());
					 }
					 catch(IllegalArgumentException t		)
					 {
						System.out.println("Not a good quantity");
					 }
					 if(desired>stock)
					 {
						label.setText("Stock too small"); 
					 }
					 else
					 {
						 label.setText("Success");
						 Order aux=new Order();
						 try {
							aux.setId(orders.get_max_id()+1);
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 aux.setProduct(product_name);
						 aux.setCustomer(customer.getText());
						 aux.setQuantity(desired);
						 products.update_stock(product_name, stock-desired);
							try {
								orders.insert_tuple(aux);
							} catch (IllegalArgumentException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (IllegalAccessException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						
						 
						 List<Object>  o_entries=new ArrayList<>((orders.getAll()));
						 orders_t.setModel(create_model(o_entries));
						 
						 List<Object>  p_entries=new ArrayList<>((products.getAll()));
						 products_t.setModel(create_model(p_entries));
					 }
					

					 
				 }
					 }
			 );
		
			input_panel.add(but);
			input_panel.add(label);
		  panel.add(input_panel);
		  aux=new JScrollPane(orders_t);
		  
		  aux.setPreferredSize(new Dimension(500,200));
		  panel.add(aux);
		 
		 
		  order_frame.add(panel);
			order_frame.setVisible(true);
			
		
		 
		 
		
	}
	void setup_info(ArrayList<JTextField> views,JTable table)
	{
		int selected=table.getSelectedRow();
	
		for(int i=0;i<table.getColumnCount();i++)
		{
			views.get(i).setText(table.getValueAt(selected, i).toString());
		}
		
	}
	private ArrayList<JTextField> create_fields(Class type,JPanel panel)
	{
		ArrayList<JTextField> display_fields=new ArrayList<JTextField>();
		 for(Field field:type.getDeclaredFields() ) //add labels to display info for editing
			 
		 {
			 JTextField aux=new JTextField();
			 aux.setMinimumSize(new Dimension(100,30));
			 aux.setPreferredSize(new Dimension(100,30));
			 display_fields.add(aux);
			 panel.add(aux);
			 
		 }
		 return display_fields;
		
	}
	private AbstractDAO return_dao(Class type)
	{
		Entry my_class=null;
		try {
			my_class =(Entry) type.newInstance();
		} catch (InstantiationException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IllegalAccessException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		AbstractDAO dao=null;
		
		 try {
			 dao=(AbstractDAO)my_class.getDAO().newInstance();
		} catch (InstantiationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 return dao;
		
	}
	private Object create_entry(ArrayList<JTextField> views,Class type)
	{
		Object aux=null;
		
			try {
				aux=type.newInstance();
			} catch (InstantiationException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (IllegalAccessException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		
		 int i=0;
		 for(Field f:type.getDeclaredFields())
		 {
			 f.setAccessible(true);	 
			 try {
				f.set(aux,Integer.parseInt(views.get(i).getText().toString()));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				 try {
					f.set(aux,views.get(i).getText().toString());
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
				
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					
				}
				
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
			
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				
			}
			 i++;
		 }
		 
		return aux; 
	}
	private JTable entries_table;
	protected void interact_db(Class type)
	 {
		
		 client_frame=new JFrame("Client Operations");
		 client_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 client_frame.setSize(700, 1000);
		 client_frame.setLocation(300, 300);	//setup the frame dimensions
		 JPanel panel= new JPanel();
		
		 dao=null;
		 dao=this.return_dao(type);
		 List<Object> a_entries=new ArrayList<>(dao.getAll());
		 
		  entries_table=new JTable(create_model(a_entries));
		 entries_table.getSelectedRow();
		 JButton but=new JButton("Delete");
		
		 panel.add(but);
		 
		 ArrayList<JTextField> display_fields=this.create_fields(type, panel);
		 but.addActionListener(new ActionListener()		//Delete function
				 {
			 public void actionPerformed(ActionEvent e)
			 {
				 int selected_row=entries_table.getSelectedRow();
				 int id_delete=Integer.parseInt(entries_table.getValueAt(selected_row, 0).toString());
				  return_dao(type).delete_by_id(id_delete);
				  List<Object>  a_entries=new ArrayList<>(dao.getAll());
				entries_table.setModel(create_model(a_entries));
			 }
				 }
		 );
		 panel.add(but);
		 but=new JButton("Insert");
		 but.addActionListener(new ActionListener()		//Insert or add
				 {
			 public void actionPerformed(ActionEvent e)
			 {
				 Object aux=create_entry(display_fields,type);
				
				 
		
				 try {
					dao.insert_tuple(aux);
					 List<Object>  a_entries=new ArrayList<>(dao.getAll());
					entries_table.setModel(create_model(a_entries));
					
				
					
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			 }
				 }
		 );
		 panel.add(but);
		 but=new JButton("Modify");
		 but.addActionListener(new ActionListener()		//Modify function
				 {
			 public void actionPerformed(ActionEvent e)
			 {
				 Object aux=create_entry(display_fields,type);
				
				 try {
					dao.update_tuple(aux);
					 List<Object>  a_entries=new ArrayList<>(dao.getAll());
						entries_table.setModel(create_model(a_entries));
						
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			 }
				 }
		 );
		 panel.add(but);
		 entries_table.addMouseListener(new MouseAdapter()
				 {
				 public void mouseClicked(MouseEvent e) {
				     if (e.getClickCount() == 1) { // check if a double click
				 
				    	 setup_info(display_fields,entries_table);
				    	 
				     }
				   }
				});
		 panel.add(new JScrollPane(entries_table));
		client_frame.add(panel);
		client_frame.setVisible(true);
		
		
	 }
	 
	 
	 
}
