package model;
import bll.ProductDAO;

public class Product extends Entry {
	private int id;
	public  Class getDAO()
	{
		return ProductDAO.class;
	}
	public int getId() {
		return id;
	}
	public void setId(int ID) {
		id=ID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQuantity() {
		return Quantity;
	}
	public void setQuantity(int quantity) {
		Quantity = quantity;
	}
	public int getPrice() {
		return Price;
	}
	public void setPrice(int price) {
		Price = price;
	}
	private String name;
	private int Quantity;
	private int Price;
}
