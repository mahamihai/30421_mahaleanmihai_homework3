package model;
import bll.OrderDAO;

public class Order extends Entry{
	
	public  Class getDAO()
	{
		return OrderDAO.class;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProduct() {
		return Product;
	}
	public void setProduct(String product) {
		Product = product;
	}
	public String getCustomer() {
		return Customer;
	}
	public void setCustomer(String customer) {
		Customer = customer;
	}
	public int getQuantity() {
		return Quantity;
	}
	public void setQuantity(int quantity) {
		Quantity = quantity;
	}
	private int id;
	private String Product;
	private String Customer;
	private int Quantity;
	
	
	
}
