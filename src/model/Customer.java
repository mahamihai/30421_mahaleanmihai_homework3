package model;
import bll.CustomerDAO;

public class Customer extends Entry {
	private int id;
	private String Name;
	public void setId(int id)
	{
		this.id=id;
	}
	public   Class getDAO()
	{
		return CustomerDAO.class;
	}
	public int getId()
	{
		return this.id;
	}
	public void setName(String name)
	{
		this.Name=name;
	}
	public String getName()
	{
		return this.Name;
	}
	
	
	
	public String toString()
	{
		return id+" "+Name+"\n";
	}
}
