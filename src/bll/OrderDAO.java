package bll;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.AbstractDAO;
import model.Customer;
import model.Order;
import model.Product;

public class OrderDAO extends AbstractDAO<Order>{
	public void create_bills() throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException
	{
		List<Order>orders=this.getAll();
		CustomerDAO customers=new CustomerDAO();
		ProductDAO products=new ProductDAO();
		int i=1;
		for(Order aux:orders)
		{
			
			System.out.println("Checking out\n:");
			String filename="Bill "+i+".txt";
			Customer customer=customers.get_by_name(aux.getCustomer());
			Product product=products.get_by_name(aux.getProduct());
			i++;
		try{
		    PrintWriter writer = new PrintWriter(filename, "UTF-8");
		    writer.println("ID:"+customer.getId());
		    writer.println("Name:"+customer.getName());
		    
		    writer.println("Bought:");
		    writer.println("Product id:"+product.getId());
		    writer.println("Product name:"+product.getName());
		    writer.println("Price/piece:"+product.getPrice());
		    writer.println("Quantity:"+aux.getQuantity());
		    writer.println("Total to pay:"+Integer.toString(product.getPrice()*aux.getQuantity()));
		    writer.close();
		} catch (IOException e) {
		   // do something
		}
		
		
	}
		}
	
	
}
