package bll;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import connection.ConnectWarehouse;
import dao.AbstractDAO;
import model.Product;

public class ProductDAO extends AbstractDAO<Product>  {
	public int get_stock(String name) throws SQLException
	{
		String query="Select quantity from Warehouse."+type.getSimpleName()+" Where name=?";
		Connection conn=null;
		System.out.println(query);
		PreparedStatement statement=null;
		conn=ConnectWarehouse.getConnection();
		ResultSet rs=null;
		try {
			statement=conn.prepareStatement(query);
			statement.setString(1, name);;
			 rs=statement.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(rs.next())
		{
			try
			{
				return rs.getInt(1);
			}
			catch(IllegalArgumentException e)
			{
				return -1;
			}
		}
		else return -2;
		
		
	}
	public void update_stock(String name,int new_s)
	{
		String query="Update Warehouse."+type.getSimpleName()+" Set quantity=? Where name=?";
		Connection conn=null;
		System.out.println(query);
		PreparedStatement statement=null;
		conn=ConnectWarehouse.getConnection();
	
		try {
			statement=conn.prepareStatement(query);
			statement.setInt(1, new_s);
			statement.setString(2, name);
			statement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
